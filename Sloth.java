public class Sloth{
	private int age; 
	private double weight; 
	private String name; 
	
	// Sloth Activities 
	public void eatLeaves(){
		double oldWeight = this.weight; 
		this.weight += 0.2; 
		System.out.println( this.name + " just ate a bunch of leaves and went from " + oldWeight + 
							" lbs to " + this.weight + " lbs!" ); 
	}

	public void climbTrees(){
		System.out.println(this.name + " just climbed a tree!" ); 
		
	}
	
	public void exercise(int hoursOfExercise){
		if(canExercise(hoursOfExercise)){
		this.weight -= (hoursOfExercise * 0.01); 
		} 
	} 
		
		/////////////////////////////////////////////////////////////////////////////////////////
	
		
		// Validation 
		
		private boolean canExercise(int hoursOfExercise){
			if(hoursOfExercise>0) {
			System.out.println(this.name + " exercised for " + hoursOfExercise + " hours"); 
				return true; 
				
			}
			System.out.println(this.name + " cannot exercise for " + hoursOfExercise + " hours"); 
			return false; 
	}
		
		
		/////////////////////////////////////////////////////////////////////////////////////////
		
		// Getters 
		
		public int getAge(){
		return this.age; 
		}
		
		public double getWeight(){
		return this.weight; 
		} 
		
		public String getName(){
		return this.name; 
		} 
		
		
		
		// Setters 
		
		/* 		public void setAge(int newAge){
				this.age = newAge; 
				} 
		*/ 
		
		public void setWeight(double newWeight){
			this.weight = newWeight; 
		}
		
		public void setName(String newName){
			this.name = newName; 
		} 
		
		/////////////////////////////////////////////////////////////////////////////////////////
	
		// Constructor
		
	public Sloth(int slothAge, double slothWeight, String slothName){
		this.age = slothAge; 
		this.weight = slothWeight;
		this.name = slothName; 
		
	} 

}

