import java.util.Scanner; 

	public class NationalPark{
				
		//---------------------------------------------------------------------------------------------//
		
		// Seperator helper function 
		public static void seperator(String symbol){
			System.out.println(); 
				
			for (int i = 0; i <50; i++){
				System.out.print(symbol); 	
			}
				
			System.out.println();
				
		} 
			
			
		public static void main(String[] args){
			Scanner scan = new Scanner(System.in);
					
			// A group of sloths is called a "bed"! 
			Sloth[] Bed = new Sloth[4]; 
					
					
		//---------------------------------------------------------------------------------------------//
		
			// Populating the bed array using constructor and user input 	
			for (int i = 0; i<Bed.length;i++){
									
				// Sloth name 
				System.out.println("What is Sloth #" + (i+1) + "'s name?"); 
				String slothName = scan.nextLine();
						
				// Sloth Age
				System.out.println("How old is " + slothName + "?"); 
				int slothAge = Integer.parseInt(scan.nextLine());
						
				// Sloth Weight 
				System.out.println("How many pounds does " + slothName + " weigh?"); 
				double slothWeight = Double.parseDouble(scan.nextLine()); 
						
				// Constructor creating Sloth Object 
				Bed[i] = new Sloth(slothAge, slothWeight, slothName);

				seperator("*"); 
					
			}
							
		//---------------------------------------------------------------------------------------------//


			// Last Sloth's detail BEFORE change 
			seperator("/"); 
			System.out.println("Sloth " + Bed.length + "'s details before the update !"); 
			System.out.println("Name:   " + Bed[(Bed.length -1)].getName()); 
			System.out.println("Weight: " + Bed[(Bed.length -1)].getWeight() + " lbs"); 
			System.out.println("Age:    " + Bed[(Bed.length -1)].getAge() + " y/o"); 
			seperator("/"); 
			
			
			// User input to update last Sloth's details 
			System.out.println("Let's update " + Bed[(Bed.length -1)].getName() + "'s details! " + '\n');
			System.out.println("What's " + Bed[(Bed.length-1)].getName() + "'s new name?");
			Bed[(Bed.length -1)].setName(scan.nextLine()); 
			System.out.println("What's " + Bed[(Bed.length-1)].getName() + "'s new weight?");
			Bed[(Bed.length -1)].setWeight(Double.parseDouble(scan.nextLine())); 
					
					
			// Last Sloth's details AFTER change
			seperator("/"); 
			System.out.println("Sloth " + Bed.length + "'s details after the update !"); 
			System.out.println("Name:   " + Bed[(Bed.length -1)].getName()); 
			System.out.println("Weight: " + Bed[(Bed.length -1)].getWeight() + " lbs"); 
			System.out.println("Age:    " + Bed[(Bed.length -1)].getAge() + " y/o"); 
			seperator("/"); 
					
		//---------------------------------------------------------------------------------------------//

			// First Sloth activity 	
			seperator("_");
			System.out.println("Looks like " + Bed[0].getName() + " is up to something!"); 
			System.out.println();
					
			Bed[0].eatLeaves(); 
			Bed[0].climbTrees(); 
					
			seperator("_"); 
				
		//---------------------------------------------------------------------------------------------//

			// Details before and after exercise! 
			System.out.println(Bed[1].getName() +" details BEFORE exercising!"); 
			System.out.println("Name:   " + Bed[1].getName()); 
			System.out.println("Weight: " + Bed[1].getWeight() + " lbs"); 
			System.out.println("Age:    " + Bed[1].getAge() + " y/o"); 
				
			seperator("_"); 
			Bed[1].exercise(10); 
			seperator("_"); 
					
			System.out.println(Bed[1].getName() +" details AFTER exercising!");
			System.out.println("Name:   " + Bed[1].getName()); 
			System.out.println("Weight: " + Bed[1].getWeight() + " lbs"); 
			System.out.println("Age:    " + Bed[1].getAge() + " y/o"); 
					
			seperator("_"); 
				
		//---------------------------------------------------------------------------------------------//
					
				
				
			} 



		} 